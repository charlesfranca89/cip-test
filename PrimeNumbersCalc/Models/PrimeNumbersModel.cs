﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrimeNumbersCalc
{
    public class PrimeNumbersModel
    {
        public int Number { get; set; }
        public List<long> Factors { get; set; }
    }
}