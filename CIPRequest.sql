create table CIPProject(
	id int primary key identity,
	name varchar(150)
);

create table CostCentre(
	id int primary key identity,
	name varchar(100)
);

create table CIPRequest(
	id int primary key identity,
	CIPProjectId int constraint CIPRequest_CIPProject foreign key references CIPProject(Id),
	CostCentreId int constraint CIPRequest_CostCentre foreign key references CostCentre(Id),
	DateCreated DateTime not null default getdate(),
	CIPShortDescription varchar(200)
);

-- 1 
select 
	request.id CIPRequestID, 
	cp.name CIPProject, 
	request.DateCreated DateCreated, 
	cc.name CostCentre, 
	request.CIPShortDescription 
from CIPRequest request
join CostCentre cc on(cc.id = request.CostCentreId)
join CIPProject cp on(cp.id = request.CIPProjectId)


-- 2
select distinct cp.name CIPProject, Count(*) [Count] 
from CIPRequest request
join CIPProject cp on(cp.id = request.CIPProjectId)
group by cp.id, cp.name

--3
select distinct cp.name CIPProject, Count(*) [Count] 
from CIPRequest request
join CIPProject cp on(cp.id = request.CIPProjectId)
WHERE MONTH(request.DateCreated) = 3 AND YEAR(request.DateCreated) = 2010
group by cp.id, cp.name